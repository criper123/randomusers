package com.example.randomuserapp.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.data.UserViewModel
import com.example.randomuserapp.models.User

class UserViewModelFactory(private val user: User?, private val userData: UserData?, private val userViewModel: UserViewModel) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserFragmentViewModel(user, userData, userViewModel) as T
    }

}