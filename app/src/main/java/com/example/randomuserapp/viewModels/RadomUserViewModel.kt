package com.example.randomuserapp.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.models.RandomUserResponse
import com.example.randomuserapp.models.SessionData
import com.example.randomuserapp.models.User
import com.example.randomuserapp.repository.RandomUserRepository

class RadomUserViewModel : ViewModel() {

    var randomUserResponse = MutableLiveData<RandomUserResponse>()
    val uiEventValue = MutableLiveData<Int>()

    init {
        val userRepository: RandomUserRepository by lazy {
            RandomUserRepository
        }
        randomUserResponse = userRepository.getRandomUsers()
    }

    fun onClickActionGridAdapter(user: User, type: Int) {
        SessionData.userFragment = user
        onActionViewModel(type)
    }

    fun onClickActionSavedUserAdapter(user: UserData) {
        SessionData.userFragmentSaved = user
        onActionViewModel(2)
    }

    private fun onActionViewModel(type: Int) {
        uiEventValue.value = type
    }

}