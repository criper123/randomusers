package com.example.randomuserapp.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.data.UserViewModel
import com.example.randomuserapp.models.User

class UserFragmentViewModel(user: User?, userData: UserData?, private val userViewModel: UserViewModel) : ViewModel() {

    private val _name = MutableLiveData(user?.name?.first ?: userData?.firstName)
    private val _phone = MutableLiveData(user?.phone ?: userData?.phone)
    private val _email =  MutableLiveData(user?.email ?: userData?.email)
    private val _address = MutableLiveData(user?.location?.street?.name ?: userData?.address)
    private val _image = MutableLiveData(user?.picture?.large ?: userData?.bigImage)
    private val _hide = MutableLiveData(if (user!=null) {
        1
    } else 0)

    var status = MutableLiveData<Boolean?>()

    val name: MutableLiveData<String?> = _name
    val phone: LiveData<String?> = _phone
    val email: LiveData<String?> = _email
    val address: MutableLiveData<String?> = _address
    val image: MutableLiveData<String?> = _image
    val hide: MutableLiveData<Int> = _hide

    fun onAddTofavorites() {
        status.value = true
        val userToSave = UserData(id = 0,
            bigImage = image.value,
            thumbnail = image.value,
            email = email.value,
            phone = phone.value,
            lastName = name.value,
            firstName = name.value,
            address = address.value)
        userViewModel.addUser(userToSave)
    }
}