package com.example.randomuserapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.randomuserapp.databinding.ItemUserGridBinding
import com.example.randomuserapp.fragments.HomeFragment
import com.example.randomuserapp.models.SessionData
import com.example.randomuserapp.viewModels.RadomUserViewModel


class GridRandomUsersAdapter(var fragment: HomeFragment) : RecyclerView.Adapter<GridRandomUsersAdapter.GridRandomViewHolder>() {

    private lateinit var vm: RadomUserViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridRandomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemUserGridBinding.inflate(layoutInflater)
        vm = ViewModelProvider(fragment).get(RadomUserViewModel::class.java)

        return GridRandomViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GridRandomViewHolder, position: Int) {
        holder.binding.homeFragmentViewModel = vm
        holder.binding.user = SessionData.users[position]
    }

    override fun getItemCount(): Int {
        return SessionData.users.size
    }

    class GridRandomViewHolder(val binding: ItemUserGridBinding) : RecyclerView.ViewHolder(binding.root)
}