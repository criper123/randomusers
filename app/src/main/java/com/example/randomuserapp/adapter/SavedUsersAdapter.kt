package com.example.randomuserapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.databinding.ItemSavedUserBinding
import com.example.randomuserapp.fragments.HomeFragment
import com.example.randomuserapp.viewModels.RadomUserViewModel

class SavedUsersAdapter (private val fragment: HomeFragment, private var data: ArrayList<UserData?>) : RecyclerView.Adapter<SavedUsersAdapter.SavedUserViewHolder>() {

    private lateinit var vm: RadomUserViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedUserViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSavedUserBinding.inflate(layoutInflater)
        vm = ViewModelProvider(fragment).get(RadomUserViewModel::class.java)
        return SavedUserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SavedUserViewHolder, position: Int) {
        holder.binding.homeFragmentViewModel = vm
        holder.binding.user = data[position]
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun updateList(newItems: List<UserData?>) {
        this.data.clear()
        this.data.addAll(newItems)
        notifyDataSetChanged()
    }
    class SavedUserViewHolder(val binding:ItemSavedUserBinding) : RecyclerView.ViewHolder(binding.root)
}