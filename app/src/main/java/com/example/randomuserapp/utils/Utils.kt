package com.example.randomuserapp.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.randomuserapp.adapter.GridRandomUsersAdapter
import com.example.randomuserapp.models.SessionData
import com.example.randomuserapp.repository.RandomUserRepository.getRandomUsersUpdate
import com.squareup.picasso.Picasso


@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Picasso.get()
        .load(imageUrl)
        .into(view)
}

@BindingAdapter(value = ["setAdapter"])
fun RecyclerView.bindRecyclerViewAdapter(adapter: RecyclerView.Adapter<*>) {

    this.run {
        if (adapter.javaClass === GridRandomUsersAdapter::class.java) {
            val layoutManager: StaggeredGridLayoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            this.layoutManager =  layoutManager
            this.setHasFixedSize(true)
            this.adapter = adapter

            this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                    var firstVisibleItems: IntArray? = null
                    val visibleItemCount = layoutManager.childCount
                    firstVisibleItems = layoutManager.findFirstVisibleItemPositions(firstVisibleItems)
                    val total = adapter.itemCount

                    if (firstVisibleItems != null && firstVisibleItems.isNotEmpty()) {
                        SessionData.pastVisibleItems = firstVisibleItems[0]
                    }

                    if (!SessionData.isLoading) {
                        if ((visibleItemCount + SessionData.pastVisibleItems) >= total) {
                            SessionData.page += 1
                            getRandomUsersUpdate(recyclerView)
                        }
                    }
                    super.onScrolled(recyclerView, dx, dy)
                }
            })
        } else {
            this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            this.setHasFixedSize(true)
            this.adapter = adapter
        }


    }
}

@BindingAdapter("app:hideIfSaved")
fun hideIfSaved(view: View, number: Int) {
    view.visibility = if (number == 0) View.GONE else View.VISIBLE
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.hideKeyboard() {
    view?.let {
        activity?.hideKeyboard(it)
    }
}