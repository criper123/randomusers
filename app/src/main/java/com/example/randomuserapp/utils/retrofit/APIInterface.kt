package com.example.randomuserapp.utils.retrofit

import com.example.randomuserapp.models.RandomUserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIInterface {
    @GET("/api/")
    fun getRadomUsers(@Query("page") page: Int?, @Query("results") results: Int?, @Query("seed") seed : String = "abc"): Call<RandomUserResponse>
}