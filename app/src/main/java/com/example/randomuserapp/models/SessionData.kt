package com.example.randomuserapp.models

import com.example.randomuserapp.data.UserData

class SessionData {

    companion object {
        val users : MutableList<User> = ArrayList()
        var page = 1
        var isLoading = false
        var pastVisibleItems = 0

        var userFragment: User? = null
        var userFragmentSaved: UserData? = null
    }
}