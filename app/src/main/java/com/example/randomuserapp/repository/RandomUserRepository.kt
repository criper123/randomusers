package com.example.randomuserapp.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.randomuserapp.models.RandomUserResponse
import com.example.randomuserapp.models.SessionData
import com.example.randomuserapp.utils.retrofit.APIClient
import com.example.randomuserapp.utils.retrofit.APIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object RandomUserRepository {

    var apiInterface: APIInterface? = null
    private val randomUserResponse = MutableLiveData<RandomUserResponse>()

    fun getRandomUsers(): MutableLiveData<RandomUserResponse> {

        SessionData.isLoading = true
        apiInterface = APIClient.getClient()?.create(APIInterface::class.java)
        val call = apiInterface!!.getRadomUsers(page = SessionData.page, results = 50)
        call.enqueue(object : Callback<RandomUserResponse> {
            @Override
            override fun onResponse(
                call: Call<RandomUserResponse>,
                response: Response<RandomUserResponse>,
            ) {
                val usersResponse = response.body()
                usersResponse?.let {
                    randomUserResponse.value = it
                }
                for (i in randomUserResponse.value!!.results) {
                    SessionData.users.add(i)
                }
            }

            @Override
            override fun onFailure(call: Call<RandomUserResponse>, t: Throwable) {
                Log.e("ERROR", t.toString());
            }
        })

        return randomUserResponse
    }

    fun getRandomUsersUpdate(param: RecyclerView): MutableLiveData<RandomUserResponse> {

        SessionData.isLoading = true
        apiInterface = APIClient.getClient()?.create(APIInterface::class.java)
        val call = apiInterface!!.getRadomUsers(page = SessionData.page, results = 50)
        call.enqueue(object : Callback<RandomUserResponse> {
            @Override
            override fun onResponse(
                call: Call<RandomUserResponse>,
                response: Response<RandomUserResponse>,
            ) {
                val usersResponse = response.body()
                usersResponse?.let {
                    randomUserResponse.value = it as RandomUserResponse

                    randomUserResponse.value = response.body()
                }
                for (i in randomUserResponse.value!!.results) {
                    SessionData.users.add(i)
                }
                param.adapter?.notifyDataSetChanged()
            }

            @Override
            override fun onFailure(call: Call<RandomUserResponse>, t: Throwable) {
                Log.e("ERROR", t.toString());
            }
        })
        return randomUserResponse
    }



}