package com.example.randomuserapp.activity

import android.view.View
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    protected var parentView : View? = null

}