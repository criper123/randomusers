package com.example.randomuserapp.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.randomuserapp.R
import com.example.randomuserapp.fragments.HomeFragment
import com.example.randomuserapp.viewModels.UserFragmentViewModel


class HomeActivity : BaseActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(UserFragmentViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        this.parentView = View.inflate(this, R.layout.activity_home, null)
        setContentView(parentView)

        val fragment = HomeFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

}