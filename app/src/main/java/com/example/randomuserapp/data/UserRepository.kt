package com.example.randomuserapp.data

import androidx.lifecycle.LiveData


class UserRepository(private val userDao: UserDao) {

    val readAllData: LiveData<List<UserData>> = userDao.readAllData()

    suspend fun addUser(user: UserData) {
        userDao.addUser(user)
    }

    suspend fun updateUser(user: UserData) {
        userDao.updateUser(user)
    }

    suspend fun deleteUser(user: UserData) {
        userDao.deleteUser(user)
    }

}