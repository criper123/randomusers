package com.example.randomuserapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.randomuserapp.R
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.data.UserViewModel
import com.example.randomuserapp.databinding.FragmentUserBindingImpl
import com.example.randomuserapp.models.User
import com.example.randomuserapp.viewModels.UserFragmentViewModel
import com.example.randomuserapp.viewModels.UserViewModelFactory
import com.google.gson.Gson

class UserFragment : BaseFragment() {

    var user: User? = null
    var savedUser: UserData? = null
    private lateinit var userViewModel: UserViewModel
    private lateinit var vm: UserFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        user = Gson().fromJson(this.arguments?.getString("user", null).toString(), User::class.java)
        savedUser = Gson().fromJson(this.arguments?.getString("savedUser", null).toString(), UserData::class.java)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        vm = ViewModelProvider(this, UserViewModelFactory(user, savedUser, userViewModel)).get(
            UserFragmentViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding: FragmentUserBindingImpl = DataBindingUtil.inflate(inflater, R.layout.fragment_user, container, false)
        binding.lifecycleOwner = this
        binding.userFragmentViewModel = vm
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.status.observe(this.activity!!, { status ->
            status?.let {
                vm.status.value = null
                Toast.makeText(this.context, "added to favorites", Toast.LENGTH_SHORT).show()
            }
        })
    }
}