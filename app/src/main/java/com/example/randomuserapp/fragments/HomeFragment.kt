package com.example.randomuserapp.fragments

import android.app.SearchManager
import android.content.Intent
import android.database.Cursor
import android.database.MatrixCursor
import android.os.Bundle
import android.provider.BaseColumns
import android.provider.ContactsContract
import android.view.*
import android.widget.AutoCompleteTextView
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.example.randomuserapp.R
import com.example.randomuserapp.adapter.GridRandomUsersAdapter
import com.example.randomuserapp.adapter.SavedUsersAdapter
import com.example.randomuserapp.data.UserData
import com.example.randomuserapp.data.UserViewModel
import com.example.randomuserapp.databinding.FragmentHomeBindingImpl
import com.example.randomuserapp.models.SessionData
import com.example.randomuserapp.utils.hideKeyboard
import com.example.randomuserapp.viewModels.RadomUserViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    var usersList: List<UserData>? = null

    var suggestions = mutableListOf<String>()

    lateinit var adapter: GridRandomUsersAdapter
    lateinit var saveUsersAdapter: SavedUsersAdapter

    private lateinit var userViewModel: UserViewModel

    lateinit var binding: FragmentHomeBindingImpl
    private lateinit var vm: RadomUserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider(this).get(RadomUserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.lifecycleOwner = this
        binding.homeFragmentViewModel = vm
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.randomUserResponse.observe(binding.lifecycleOwner!!, {
           vm.uiEventValue.value = 3
        })

        vm.uiEventValue.observe(binding.lifecycleOwner!!, {
            when (it) {
                0 -> {
                    val intent = Intent(Intent.ACTION_INSERT)
                    intent.type = ContactsContract.Contacts.CONTENT_TYPE
                    intent.putExtra(ContactsContract.Intents.Insert.NAME, SessionData.userFragment?.name?.first + " " + SessionData.userFragment?.name?.last)
                    intent.putExtra(ContactsContract.Intents.Insert.PHONE, SessionData.userFragment?.phone)
                    intent.putExtra(ContactsContract.Intents.Insert.EMAIL, SessionData.userFragment?.email)
                    activity!!.startActivity(intent)
                }
                1 -> {
                    val bundle = Bundle()
                    bundle.putString("user", Gson().toJson(SessionData.userFragment))
                    val fragment = UserFragment()
                    fragment.arguments = bundle
                    this.activity!!.supportFragmentManager.beginTransaction()
                        .add(R.id.container, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit()
                }
                2 -> {
                    val bundle = Bundle()
                    bundle.putString("savedUser", Gson().toJson(SessionData.userFragmentSaved))
                    val fragment = UserFragment()
                    fragment.arguments = bundle
                    this.activity!!.supportFragmentManager.beginTransaction()
                        .add(R.id.container, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit()
                }
                3 -> {
                    SessionData.isLoading = false
                    if (::adapter.isInitialized) {
                        adapter.notifyDataSetChanged()
                    } else {
                        adapter = GridRandomUsersAdapter(this)
                        binding.adapter = adapter
                    }
                }
            }
        })

        saveUsersAdapter = SavedUsersAdapter(this, ArrayList())
        binding.savedUseradapter = saveUsersAdapter

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        userViewModel.readAllData.observe(viewLifecycleOwner, { users ->
            usersList = users
            if (!users.isNullOrEmpty()) {
                suggestions.clear()
                for (i in usersList!!) {
                    suggestions.add(i.firstName.toString())
                }
                saved_list.visibility = View.VISIBLE
            } else {
                saved_list.visibility = View.GONE
            }
            saveUsersAdapter.updateList(users)
        })

        adapter = GridRandomUsersAdapter(this)
        binding.adapter = adapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.actionbar_menu, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        searchView.queryHint = getString(R.string.search)
        searchView.findViewById<AutoCompleteTextView>(R.id.search_src_text).threshold = 1

        val from = arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1)
        val to = intArrayOf(R.id.item_label)
        val cursorAdapter = SimpleCursorAdapter(context,
            R.layout.suggestion,
            null,
            from,
            to,
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)

        searchView.suggestionsAdapter = cursorAdapter

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                hideKeyboard()
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                val cursor = MatrixCursor(arrayOf(BaseColumns._ID,
                    SearchManager.SUGGEST_COLUMN_TEXT_1))
                query?.let {
                    suggestions.forEachIndexed { index, suggestion ->
                        if (suggestion.contains(query, true))
                            cursor.addRow(arrayOf(index, suggestion))
                    }
                }

                cursorAdapter.changeCursor(cursor)
                return true
            }
        })

        searchView.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean {
                return false
            }

            override fun onSuggestionClick(position: Int): Boolean {
                hideKeyboard()
                val cursor = searchView.suggestionsAdapter.getItem(position) as Cursor
                val selection = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1))
                searchView.setQuery(selection, false)

                val user = usersList!!.first {
                    it.firstName == selection
                }

                val bundle = Bundle()
                bundle.putString("savedUser", Gson().toJson(user))
                val fragment = UserFragment()
                fragment.arguments = bundle
                activity!!.supportFragmentManager.beginTransaction()
                    .add(R.id.container, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit()

                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }
}